package com.triangle.com.humano.Interface;

        import com.triangle.com.humano.Model.AttendanceModel;
        import com.triangle.com.humano.Model.CheckinModel;
        import com.triangle.com.humano.Model.OutsideList;
        import com.triangle.com.humano.Model.OutsideModel;
        import com.triangle.com.humano.Model.UserModel;

        import retrofit2.Call;
        import retrofit2.http.Field;
        import retrofit2.http.FormUrlEncoded;
        import retrofit2.http.POST;

public interface APIInterface {

    @FormUrlEncoded
    @POST("api/login.php")
    Call<UserModel>getLogin(@Field("username") String username,@Field("password") String password);




    @FormUrlEncoded
    @POST("api/getuserdata.php")
    Call<UserModel>getUserData(@Field("username") String username,@Field("token") String token);


    @FormUrlEncoded
    @POST("api/getlocationoutside.php")
    Call<AttendanceModel>getLocationside(@Field("lat") String username, @Field("token") String token);



    @FormUrlEncoded
    @POST("api/documant_onside.php")
    Call<OutsideList>getDocumant_onside(@Field("month") String month, @Field("year") String year, @Field("username") String username, @Field("token") String token);



    @FormUrlEncoded
    @POST("api/onside_request.php")
    Call<CheckinModel>setonside_request(@Field("lat") String lat, @Field("lng") String lng, @Field("date_start") String date_start, @Field("date_end") String date_end, @Field("time_start") String time_start, @Field("time_end") String time_end, @Field("lang") String lang , @Field("username") String username , @Field("token") String token);


    @FormUrlEncoded
    @POST("api/setclockin.php")
    Call<AttendanceModel>setClockin(@Field("lat") String lat, @Field("lng") String lng, @Field("status") String status, @Field("clocktype") String clocktype, @Field("username") String username, @Field("token") String token);


}




