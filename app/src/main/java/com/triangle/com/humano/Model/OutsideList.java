package com.triangle.com.humano.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OutsideList {

    @SerializedName("data")
    private ArrayList<OutsideModel> data;

    public ArrayList<OutsideModel> getOutsideList() {
        return data;
    }

    public void setOutsideList(ArrayList<OutsideModel> data) {
        this.data = data;
    }
}
