package com.triangle.com.humano.Scences.Attendance;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.triangle.com.humano.R;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class AttendanceActivity extends AppCompatActivity {

    RelativeLayout borderClockin, borderClockout;
    FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        borderClockin = findViewById(R.id.border_clockin);
        borderClockout = findViewById(R.id.border_clockout);

        getSupportActionBar().setTitle("Attendance");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        client = LocationServices.getFusedLocationProviderClient(this);
        requestPermission();

        borderClockin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), ClockInActivity.class);
                AttendanceActivity.this.startActivity(intent);

                if (ActivityCompat.checkSelfPermission(AttendanceActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
            }
        });


        borderClockout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClockOutActivity.class);
                startActivity(intent);
            }
        });




    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1 );
    }

}
