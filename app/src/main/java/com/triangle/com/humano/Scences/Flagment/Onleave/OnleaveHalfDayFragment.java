package com.triangle.com.humano.Scences.Flagment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.LanguageActivity;
import com.triangle.com.humano.Scences.Outside.OutSlideActivity;

import java.io.IOException;
import java.util.Calendar;

import static com.triangle.com.humano.R.string.changLanguage;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnleaveHalfDayFragment extends Fragment {

    private static final int IMG_REQUEST = 777;
    private Bitmap bitmap;

    Calendar calendar;
    DatePickerDialog datePickerDialog;

    RelativeLayout selectDate, selectTime, selectReason, selectFile;
    TextView selectDateResult, selectTimeResult, selectReasonResult, selectFileResult, textView;
    ImageView imageView;

    int day, month, year;




    public OnleaveHalfDayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_onleave_half_day, container, false);


        selectDate = view.findViewById(R.id.onleave_relativelaout_halfday_1);
        selectTime = view.findViewById(R.id.onleave_relativelaout_halfday_4);
        selectReason = view.findViewById(R.id.onleave_relativelaout_halfday_2);
        selectFile = view.findViewById(R.id.onleave_relativelaout_halfday_3);

        selectDateResult = view.findViewById(R.id.select_date_halfday);
        selectTimeResult = view.findViewById(R.id.select_time_halfday);
        selectReasonResult = view.findViewById(R.id.select_reason_halfday);
        selectFileResult = view.findViewById(R.id.select_file_halfday);


        imageView = view.findViewById(R.id.imageView_halfday);


        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                calendar = Calendar.getInstance();

                day = calendar.get(Calendar.DAY_OF_MONTH);
                month = calendar.get(Calendar.MONTH);
                year = calendar.get(Calendar.YEAR);

                datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        selectDateResult.setText(dayOfMonth + "/" +(month+1)+ "/" + year);

                    }
                }, day, month, year);
                datePickerDialog.show();
            }
        });


        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              showChangLanguageDialog();


            }
        });


        selectReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


        selectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });








        return view;
    }


    private void showChangLanguageDialog() {
        final String[] listItem = {"ครึ่งเช้า","ครึ่งบ่าย"};
        android.support.v7.app.AlertDialog.Builder mBuilder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        mBuilder.setTitle("Choose Time...");
//        mBuilder.setTitle(R.string.app_name);
        mBuilder.setSingleChoiceItems(listItem, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (i == 0){

                    selectTimeResult.setText("ครึ่งเช้า");


                } else if (i == 1){

                    selectTimeResult.setText("ครึ่งบ่าย");

                }

                dialogInterface.dismiss();

            }
        });

        android.support.v7.app.AlertDialog alertDialog =mBuilder.create();
        alertDialog.show();
    }



    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View subView = inflater.inflate(R.layout.dialog_onleave_reason, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Reason");
        builder.setMessage("Leave reason Message");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectReasonResult.setText(subEditText.getText().toString());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

    private void  selectImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {

            Uri path = data.getData();

            try {
                bitmap =  MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),path);
                imageView.setImageBitmap(bitmap);

                String resName = getResources().getResourceEntryName(R.id.imageView_hour);

                selectFileResult.setText(""+resName);



            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
