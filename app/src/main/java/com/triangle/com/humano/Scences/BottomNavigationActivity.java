package com.triangle.com.humano.Scences;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.Flagment.DashBoardFragment;
import com.triangle.com.humano.Scences.Flagment.EsmFragment;
import com.triangle.com.humano.Scences.Flagment.MoreFragment;
import com.triangle.com.humano.Scences.Flagment.NewsFragment;
import com.triangle.com.humano.Scences.Flagment.ProfileFragment;


public class BottomNavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);





        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.add(R.id.fragment_container,new EsmFragment());
        fragmentTransaction.addToBackStack(null).commit();



        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);



    }








    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectFragment = null ;

                    switch (menuItem.getItemId()){

                        case R.id.nav_dashboard:

                            selectFragment = new DashBoardFragment();
                            getSupportActionBar().setTitle("DashBoard");

                            break;

                        case R.id.nav_esm:

                            selectFragment = new EsmFragment();
                            getSupportActionBar().setTitle("Esm");
                            break;

                        case R.id.nav_profile:

                            selectFragment = new ProfileFragment();
                            getSupportActionBar().setTitle("Profile");
                            break;

                        case R.id.nav_news:

                            selectFragment = new NewsFragment();
                            getSupportActionBar().setTitle("News");
                            break;

                        case R.id.nav_more:

                            selectFragment = new MoreFragment();
                            getSupportActionBar().setTitle("More");
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectFragment).addToBackStack(null).commit();
                    return  true;
                }


            };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
