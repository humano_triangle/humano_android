package com.triangle.com.humano.Scences.Outside;


import android.app.DatePickerDialog;
import android.content.Intent;

import android.content.pm.PackageManager;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.Button;

import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.triangle.com.humano.Adapter.OutsideAdapter;
import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.Interface.APIInterface;
import com.triangle.com.humano.Model.OutsideList;
import com.triangle.com.humano.Model.OutsideModel;
import com.triangle.com.humano.Network.RetrofitInstance;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.MonthYearPickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class MainOutSideActivity extends AppCompatActivity implements OutsideAdapter.OnItemClickListener {

    public static final String EXTRA_LAT = "lat";
    public static final String EXTRA_LNG = "lng";
    public static final String EXTRA_DATE = "date_Date";
    public static final String EXTRA_TIME = "date_Time";


    private OutsideAdapter mOutsideAdapter, outsideAdapter;

    private RecyclerView recyclerView;
    private ArrayList<OutsideModel> dataList;



    String monthYearStr, monthSelect, yearSelect;


    Button  selectMonthYear;
    TextView yearMonth;
    Helper helper;

    SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");

    private boolean isOpen = false;


    RelativeLayout  outside, checkout, rootLayout;
    private FusedLocationProviderClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_out_side);

        outside =  findViewById(R.id.border_outside);
        checkout = findViewById(R.id.border_checkout);

        yearMonth = findViewById(R.id.month_year);
        selectMonthYear = findViewById(R.id.search_month);

        helper = new Helper(getApplicationContext());

        recyclerView = findViewById(R.id.recycler_view_employee_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));





        getSupportActionBar().setTitle("Outside");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);



        client = LocationServices.getFusedLocationProviderClient(this);
        requestPermission();



        outside.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainOutSideActivity.this, OutSlideActivity.class);
                startActivity(intent);


            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainOutSideActivity.this, CheckOutActivity.class);
                MainOutSideActivity.this.startActivity(intent);
            }
        });




        selectMonthYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
                pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int i2) {

                        monthYearStr = year + "-" + (month + 1) + "-" + i2;

                        System.out.println(" "+year);
                        System.out.println(" "+month);

                        yearSelect = "" + year;
                        monthSelect = "" + month;

                        fetchiUserData(monthSelect, yearSelect);




                        yearMonth.setText(formatMonthYear(monthYearStr));
                    }
                });
                pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");
            }
        });










    }

    String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

//    private void setupComponent() {
//
//
//        String prefix = "http://"+helper.getHostData();
//        String empName = helper.getUserData().getName();
//        String empSName = helper.getUserData().getSname();
//        String empId = helper.getUserData().getEmcode();
//
//
//
//
//    }





    private void fetchiUserData(String monthPicker, String  yearPicker ) {
        final String month = ""+monthPicker;
        final String year = ""+yearPicker;


        APIInterface apiInterface = RetrofitInstance
                .getRetrofitInstance(getApplicationContext())
                .create(APIInterface.class);

        Call<OutsideList> call = apiInterface.getDocumant_onside(month, year, helper.getUsername(), helper.getUserData().getToken());
        call.enqueue(new Callback<OutsideList>() {
            @Override
            public void onResponse(Call<OutsideList> call, Response<OutsideList> response) {

                if (response.isSuccessful()) {

//                    generateOutside(response.body().getOutsideList());


                    OutsideList outsideList = response.body();
                    dataList = outsideList.getOutsideList();

                    mOutsideAdapter = new OutsideAdapter(MainOutSideActivity.this, dataList);
                    recyclerView.setAdapter(mOutsideAdapter);
                    mOutsideAdapter.setOnItemClickListener(MainOutSideActivity.this);





                } else {
                    Toast.makeText(getApplicationContext(), "Token require", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<OutsideList> call, Throwable t) {

                System.out.println(" "+call);
                System.out.println(" "+t);
                System.out.println(" "+helper.getUsername());
                System.out.println(" "+helper.getUserData().getToken());
                System.out.println(" "+year);
                System.out.println(" "+month);

                Toast.makeText((getApplicationContext()), "Internal server error", Toast.LENGTH_SHORT).show();

            }


        });

    }



//    private void generateOutside(ArrayList<OutsideModel> empDataList) {
//        recyclerView = findViewById(R.id.recycler_view_employee_list);
//
//        outsideAdapter = new OutsideAdapter(empDataList);
//
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainOutSideActivity.this);
//
//        recyclerView.setLayoutManager(layoutManager);
//
//        recyclerView.setAdapter(outsideAdapter);
//    }


    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1 );
    }


    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, CheckInActivity.class);
        OutsideModel clickedItem = dataList.get(position);


        intent.putExtra(EXTRA_LAT, clickedItem.getLat());
        intent.putExtra(EXTRA_LNG, clickedItem.getLng());
        intent.putExtra(EXTRA_DATE, clickedItem.getDate_Date());
        intent.putExtra(EXTRA_TIME, clickedItem.getDate_Time());

        startActivity(intent);




    }
}
