package com.triangle.com.humano.Scences;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.triangle.com.humano.Model.SalaryModel;
import com.triangle.com.humano.R;

import java.util.ArrayList;

public class SalaryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salary);


        getSupportActionBar().setTitle("Salary");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BarChart chart = (BarChart) findViewById(R.id.bar_chart);






        final ArrayList<SalaryModel> listStudent = SalaryModel.getSampleStudentData(30);

        final ArrayList<BarEntry> entries = new ArrayList<>();
        int index = 0;
        for (SalaryModel student : listStudent) {
            entries.add(new BarEntry(index, student.getScore()));
            index++;
        }

        BarDataSet dataset = new BarDataSet(entries, "#");
        dataset.setValueTextSize(8);
        dataset.setValueTextColor(Color.WHITE);
        dataset.setColors(ColorTemplate.LIBERTY_COLORS); // set the color




        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataset);
        dataset.setHighlightEnabled(false);

        BarData data = new BarData(dataSets);
        chart.setData(data);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelRotationAngle(80);
        chart.getXAxis().setDrawGridLines(false);
        chart.getAxisLeft().setDrawGridLines(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.setEnabled(false);

        final XAxis xAxis = chart.getXAxis();
        xAxis.setTextSize(8);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new AxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Log.d("benznest", "value = " + value);
                if (value < 0 || value >= listStudent.size()) {
                    return "";
                }
                return listStudent.get((int) value).getName();
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });

        YAxis RightAxis = chart.getAxisRight();
        RightAxis.setEnabled(false);

        chart.animateY(3000);



    }
}
