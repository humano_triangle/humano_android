package com.triangle.com.humano.Scences.Flagment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.Interface.APIInterface;
import com.triangle.com.humano.Model.UserModel;
import com.triangle.com.humano.Network.RetrofitInstance;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.MainActivity;
import com.triangle.com.humano.Scences.SalaryActivity;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    Helper helper;
    TextView empNameTextView, empIdTextView, empPhoneTextView,empSalaryView;
    CircleImageView  empimageView;
    RelativeLayout inCome;
    private SwipeRefreshLayout mSwipeRefresh;
    private HorizontalScrollView horizontalScrollView;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mSwipeRefresh = rootView.findViewById(R.id.swipe_refresh_layout);
        horizontalScrollView = rootView.findViewById(R.id.horizontal_scrollview);

        inCome = rootView.findViewById(R.id.income);


        empimageView = rootView.findViewById(R.id.emp_imageView);
        empIdTextView = rootView.findViewById(R.id.emp_id_text);
        empNameTextView = rootView.findViewById(R.id.emp_name_textView);
//        empPhoneTextView = rootView.findViewById(R.id.emp_phone);
//        empSalaryView = rootView.findViewById(R.id.emp_salary_text);

        helper = new Helper(getActivity());
        fetchiUserData();
        mSwipeRefresh.setRefreshing(true);
        setupComponent();


        horizontalScrollView.setSmoothScrollingEnabled(true);
        horizontalScrollView.setHorizontalScrollBarEnabled(false);

        inCome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SalaryActivity.class);
                startActivity(intent);
            }
        });



        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchiUserData();
                setupComponent();

            }
        });


        return rootView;
    }



    private void setupComponent() {


        String prefix = "http://"+helper.getHostData();
        String empName = helper.getUserData().getName();
        String empSName = helper.getUserData().getSname();
        String empId = helper.getUserData().getEmcode();
//        String empPhone = helper.getUserData().getPhoneNumber();
        String empImageURL = prefix+helper.getUserData().getImage();
//        Double empSalary = helper.getUserData().getSalary();

        System.out.println("imagePath: "+empImageURL);

        Picasso.get().load(empImageURL).into(empimageView);
//        empPhoneTextView.setText("Phone: "+empPhone);
        empNameTextView.setText(empName + " " + empSName);
        empIdTextView.setText(empId);
//        empSalaryView.setText(""+empSalary);




    }


    private void fetchiUserData() {

        APIInterface apiInterface = RetrofitInstance
                .getRetrofitInstance(getActivity())
                .create(APIInterface.class);

        Call<UserModel> call = apiInterface.getUserData(helper.getUsername(), helper.getUserData().getToken());
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                System.out.println(" "+response);
                System.out.println(" "+call);

                mSwipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    helper.setUserData(response.body());
                    UserModel model = helper.getUserData();
//                    String model1 = helper.getUsername();


                    System.out.println(""+helper.getUsername());



                }else {
                    Toast.makeText(getActivity(), "Token require", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {

                Toast.makeText(getActivity(),"Internal server error", Toast.LENGTH_SHORT).show();

            }
        });



    }

}