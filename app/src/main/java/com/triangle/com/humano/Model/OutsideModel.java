package com.triangle.com.humano.Model;

import com.google.gson.annotations.SerializedName;

public class OutsideModel {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lng")
    private String lng;

    @SerializedName("date_Date")
    private String date_Date;

    @SerializedName("date_Time")
    private String  date_Time;


//    @SerializedName("date_start")
//    private String date_start;
//
//    @SerializedName("date_end")
//    private String date_end;
//
//    @SerializedName("time_start")
//    private String time_start;
//
//    @SerializedName("time_end")
//    private String time_end;


    @SerializedName("lang")
    private String lang;

    public OutsideModel(String lat, String lng, String date_Date, String date_Time, String lang){

        this.lat = lat;
        this.lng = lng;
        this.date_Date = date_Date;
        this.date_Time = date_Time;
        this.lang = lang;





    }

    public String getLat() { return lat; }
    public void setLat(String lat) { this.lat = lat; }

    public String getLng() { return lng; }
    public void setLng(String lng) { this.lng = lng; }

    public String getDate_Date() { return date_Date; }
    public void setDate_Date(String date_Date) { this.date_Date = date_Date; }

    public String getDate_Time() { return date_Time; }
    public void setDate_Time(String date_Time) { this.date_Time = date_Time; }

    public String getLang() { return lang; }
    public void setLang(String lang) { this.lang = lang; }







}
