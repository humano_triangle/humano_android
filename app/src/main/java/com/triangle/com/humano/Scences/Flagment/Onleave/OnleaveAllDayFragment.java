package com.triangle.com.humano.Scences.Flagment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.triangle.com.humano.R;

import android.widget.Toast;

import java.io.IOException;
import java.lang.String;

import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnleaveAllDayFragment extends Fragment implements  DatePickerDialog.OnDateSetListener {

    RelativeLayout selectDate, selectResson, selectFile;
    String startDate, startTime, endDate, endTime;
    ImageView imageView;

    TextView selectDateResult, selectReasonResult, selectFileResult;
    private Bitmap bitmap;

    private static final int IMG_REQUEST = 777;


//    TextView dateStartResult, monthStartResult, yearStartResult, timeStartResult;
//    TextView dateEndResult, monthEndResult, yearEndResult, timeEndResult;
//    TextView dateStart, timeStart ;

    public OnleaveAllDayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_onleave_all_day, container, false);

        selectDate = view.findViewById(R.id.onleave_relativelaout_allday_1);
        selectDateResult = view.findViewById(R.id.select_date_multi);

        selectResson = view.findViewById(R.id.onleave_relativelaout_allday_2);
        selectReasonResult = view.findViewById(R.id.select_reason_allday);

        selectFile = view.findViewById(R.id.onleave_relativelaout_allday_3);
        selectFileResult = view.findViewById(R.id.select_file_multi);

        imageView = view.findViewById(R.id.imageView_allday);



        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener) OnleaveAllDayFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(true);
                dpd.show(getActivity().getFragmentManager(),"Datepickerdialog");



            }
        });

        selectResson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        selectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();

            }
        });




        return view;
    }



    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View subView = inflater.inflate(R.layout.dialog_onleave_reason, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Reason");
        builder.setMessage("Leave reason Message");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectReasonResult.setText(subEditText.getText().toString());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

    private void  selectImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {

            Uri path = data.getData();

            try {
                bitmap =  MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),path);
                imageView.setImageBitmap(bitmap);

                String resName = getResources().getResourceEntryName(R.id.imageView_hour);

                selectFileResult.setText(""+resName);



            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }






    @Override
    public void onResume() {
        super.onResume();
        com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = (com.borax12.materialdaterangepicker.date.DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String dateStart =  dayOfMonth + "/" + (++monthOfYear) + "/" + year ;
        String dateEnd = dayOfMonthEnd + "/" + (++monthOfYearEnd) + "/" + yearEnd;
        String dayStart = ""+dayOfMonth;
        String dayEnd = ""+dayOfMonthEnd;
        String monthStart = ""+monthOfYear;
        String monthEnd = ""+monthOfYearEnd;
        String yearStart = ""+year;
        String yearsEnd = ""+yearEnd;


        selectDateResult.setText(dateStart+" - "+dateEnd);
//
//        dateStartResult.setText(dayStart);
//        monthStartResult.setText(monthEnd);
//        yearStartResult.setText(yearStart);
//        startDate = ""+dateStart;



//        dateEndResult.setText(dayEnd);
//        monthEndResult.setText(monthEnd);
//        yearEndResult.setText(yearsEnd);
//        endDate = ""+dateEnd;





    }
}
