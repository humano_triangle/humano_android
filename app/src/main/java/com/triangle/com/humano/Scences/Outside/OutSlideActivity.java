package com.triangle.com.humano.Scences.Outside;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.Interface.APIInterface;
import com.triangle.com.humano.Model.CheckinModel;
import com.triangle.com.humano.Model.OutsideModel;
import com.triangle.com.humano.Network.RetrofitInstance;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.PlaceAutocompleteAdapter;

import java.io.IOException;
//import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutSlideActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 17f;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-40, -168), new LatLng(71, 136));
    //
    private AutoCompleteTextView mSearchText;
    private ImageView mGps;
    //
    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;
    private GoogleApiClient mGoogleApiClient;





    String startDate, startTime, endDate, endTime;
    private LatLng placeLatLng;



    RelativeLayout goLocation, selectDate, selectDateTimeEnd;
    Marker marker, mVisitingMarker;
    TextView locationLat, locationLong, showLocation, selectTime;
    TextView dateStart, timeStart ;
    TextView dateStartResult, monthStartResult, yearStartResult, timeStartResult;
    TextView dateEndResult, monthEndResult, yearEndResult, timeEndResult;
    Button comFirm;
    Helper helper;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_slide);

        dateStart = findViewById(R.id.date);
        timeStart = findViewById(R.id.timeStart);

        mSearchText = findViewById(R.id.input_search);
        goLocation = findViewById(R.id.border_gps);

        comFirm = findViewById(R.id.btn_confirm);

        selectDate = findViewById(R.id.border_date);
        selectTime = findViewById(R.id.timeStart);

        dateStartResult = findViewById(R.id.date);
        monthStartResult = findViewById(R.id.month);
        yearStartResult = findViewById(R.id.year);
        timeStartResult = findViewById(R.id.timeStart);

        dateEndResult = findViewById(R.id.dateEnd);
        monthEndResult = findViewById(R.id.monthEnd);
        yearEndResult = findViewById(R.id.yearEnd);
        timeEndResult = findViewById(R.id.timeEnd);
        helper = new Helper(getApplication());


        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        (com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener) OutSlideActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setAutoHighlight(true);
                dpd.show(getFragmentManager(), "Datepickerdialog");

            }
        });


        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.time.TimePickerDialog tpd = com.borax12.materialdaterangepicker.time.TimePickerDialog.newInstance(
                        (com.borax12.materialdaterangepicker.time.TimePickerDialog.OnTimeSetListener) OutSlideActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("TimePicker", "Dialog was cancelled");
                    }
                });
                tpd.show(getFragmentManager(), "Timepickerdialog");


            }
        });


        comFirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (placeLatLng != null) {
                    setOutsideApi(placeLatLng, startDate, endDate, startTime, endTime);
                } else {
                    Toast.makeText(getApplicationContext(), "Internal server error", Toast.LENGTH_SHORT).show();

                }


            }
        });

        getLocationPermission();

//        //////////////////////////////////////////////////// date /////////////////////////////////////////////////////////////
//
//        Calendar c = Calendar.getInstance();
//        //Date
//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
//        String formattedDate = dateFormat.format(c.getTime());
//
//        SimpleDateFormat monthFormat = new SimpleDateFormat("MMMMM");
//        String formattedMonth = monthFormat.format(c.getTime());
//
//        SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
//        String formattedYear = yearFormat.format(c.getTime());
//
//        //Time
//        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
//        String formattedTime = timeFormat.format(c.getTime());
//
//
//
//
//        TextView date = (TextView) findViewById(R.id.date);
//        TextView month = (TextView) findViewById(R.id.month);
//        TextView year = (TextView) findViewById(R.id.year);
//        TextView time = (TextView) findViewById(R.id.timeStart);
//
//
////        result.setText(" " + formattedDate);
//
//
//        year.setText(" " + formattedYear);
//        month.setText(" " + formattedMonth);
//        date.setText(" " + formattedDate);
//        time.setText(" " + formattedTime);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is Ready", Toast.LENGTH_SHORT).show();

        mMap = googleMap;
        if (mLocationPermissionsGranted) {
            getDeviceLocation();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(false);
        }


        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {

                MarkerOptions markerOptions = new MarkerOptions();
                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(point);
                circleOptions.strokeColor(Color.GREEN);
                circleOptions.radius(50);

                markerOptions.position(point);
                markerOptions.title(point.latitude + " : " + point.longitude);
                mMap.clear();
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(point,17));
                mMap.addMarker(markerOptions);
                mMap.addCircle(circleOptions);
                placeLatLng = new LatLng(point.latitude, point.longitude);
//                showLocation.setText(point.latitude + " : " + point.longitude);


            }
        });









        init();
    }

    private void init(){

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, mGoogleApiClient, LAT_LNG_BOUNDS, null);

        mSearchText.setAdapter(placeAutocompleteAdapter);

        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || keyEvent.getAction() == KeyEvent.ACTION_DOWN || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {

                    geoLocation();

                    closeKeyboard();
                }
                return false;

            }
        });



        goLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });


    }


    private void geoLocation(){
        String searchString = mSearchText.getText().toString();

        Geocoder geocoder = new Geocoder(OutSlideActivity.this);
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(searchString,1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (list.size()> 0 ){
            Address address = list.get(0);



            moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM, address.getAddressLine(0));

//            locationLat = findViewById(R.id.location_lat);
//            locationLong = findViewById(R.id.location_long);

//            locationLat.append(" " + address.getLatitude() );
//            locationLong.append(" " + address.getLongitude());

            placeLatLng = new LatLng(address.getLatitude(), address.getLongitude());

        }


    }


    private  void getDeviceLocation(){
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (mLocationPermissionsGranted){
                Task location = mFusedLocationClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                            Location currentLocation = (Location) task.getResult();

                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);

                        }else {
                            Toast.makeText(OutSlideActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        }catch (SecurityException e){

        }
    }


    private void moveCamera(LatLng latLng, float defaultZoom) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,defaultZoom));

    }


    private void moveCamera(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom));

        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(title);

        CircleOptions circleOptions = new CircleOptions()
                .radius(50)
                .center(latLng)
                .strokeColor(Color.GREEN)
                ;

        mMap.addMarker(options);
        mMap.addCircle(circleOptions);





    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length >0 ){
                    for (int i = 0;i < grantResults.length; i++){
                        if ( grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionsGranted = false;
                            return;
                        }

                    }
                    mLocationPermissionsGranted = true;
                    initMap();

                }

            }
        }
    }

    private void initMap(){

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(OutSlideActivity.this);
    }
    private void getLocationPermission(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),COURSE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                mLocationPermissionsGranted = true;
                initMap();

            }else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }

    }
    private void closeKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null){
            InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public void hideKeyboard(View view){
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    @Override
    public void onResume() {
        super.onResume();
        com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = (com.borax12.materialdaterangepicker.date.DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }



    @Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String dateStart =  dayOfMonth + "/" + (++monthOfYear) + "/" + year ;
        String dateEnd = dayOfMonthEnd + "/" + (++monthOfYearEnd) + "/" + yearEnd;
        String dayStart = ""+dayOfMonth;
        String dayEnd = ""+dayOfMonthEnd;
        String monthStart = ""+monthOfYear;
        String monthEnd = ""+monthOfYearEnd;
        String yearStart = ""+year;
        String yearsEnd = ""+yearEnd;


        dateStartResult.setText(dayStart);
        monthStartResult.setText(monthEnd);
        yearStartResult.setText(yearStart);
        startDate = ""+dateStart;



        dateEndResult.setText(dayEnd);
        monthEndResult.setText(monthEnd);
        yearEndResult.setText(yearsEnd);
        endDate = ""+dateEnd;

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {

        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0" + hourOfDayEnd : "" + hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0" + minuteEnd : "" + minuteEnd;
        String time = hourString + ":" + minuteString ;
        String timeend = hourStringEnd + ":" + minuteStringEnd;

        timeStartResult.setText(time);
        startTime = ""+time;

        timeEndResult.setText(timeend);
        endTime = ""+timeend;
    }





    private void setOutsideApi(LatLng latLng, String startDate, String endDate, String startTime, String endTime ) {

        final String lat = ""+latLng.latitude;
        final String lng = ""+latLng.longitude;
        final String date_start = "" + startDate;
        final String date_end = "" + endDate;
        final String time_start = "" + startTime;
        final String time_end = "" + endTime;
        final String lang = "EN";



        APIInterface apiInterface = RetrofitInstance
                .getRetrofitInstance(getApplicationContext())
                .create(APIInterface.class);
        Call<CheckinModel> call = apiInterface.setonside_request(lat, lng, date_start, date_end, time_start, time_end, lang, helper.getUsername(), helper.getUserData().getToken() );
        call.enqueue(new Callback<CheckinModel>() {
            @Override
            public void onResponse(Call<CheckinModel> call, Response<CheckinModel> response) {
                if (response.isSuccessful()){
                    OutSlideActivity.super.onBackPressed();
                    Toast.makeText(getApplicationContext(), "Sucess", Toast.LENGTH_SHORT).show();


                    System.out.println(" "+helper.getUserData().getToken());
                    System.out.println(" "+helper.getUsername());
                    System.out.println(" "+lat);
                    System.out.println(" "+lng);
                    System.out.println("date: "+date_start);
                    System.out.println("time: "+time_start);
                    System.out.println("dateEnd: "+date_end);
                    System.out.println("timeEnd: "+time_end);
                    System.out.println("Language: "+lang);

                }else {
                    Toast.makeText(getApplicationContext(), "Token require", Toast.LENGTH_SHORT).show();
                    OutSlideActivity.super.onBackPressed();

                    System.out.println(" "+helper.getUserData().getToken());
                    System.out.println(" "+helper.getUsername());
                    System.out.println(" "+lat);
                    System.out.println(" "+lng);
                    System.out.println("date: "+date_start);
                    System.out.println("time: "+time_start);
                    System.out.println("Language: "+lang);
                }

            }

            @Override
            public void onFailure(Call<CheckinModel> call, Throwable t) {

                Toast.makeText(getApplicationContext(),"Internal server error", Toast.LENGTH_SHORT).show();
                OutSlideActivity.super.onBackPressed();

            }
        });



    }



}