package com.triangle.com.humano.Scences;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.Interface.APIInterface;
import com.triangle.com.humano.Model.UserModel;
import com.triangle.com.humano.Network.RetrofitInstance;
import com.triangle.com.humano.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class  SplashActivity extends Activity {

    // Splash screen timer
    private Helper helper;
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                helper = new Helper(getApplicationContext());
                UserModel userModel = helper.getUserData();

                if (userModel != null) {
                    Toast.makeText(SplashActivity.this,""+userModel.getName(), Toast.LENGTH_LONG).show();
                    APIInterface apiInterface = RetrofitInstance
                            .getRetrofitInstance(getApplicationContext())
                            .create(APIInterface.class);
                    Call<UserModel> call = apiInterface.getUserData(helper.getUsername(),helper.getUserData().getToken());
                    call.enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            //TODO something...
                            if(response.isSuccessful()){
                                helper.setUserData(response.body());
                                Intent intent = new Intent(SplashActivity.this, BottomNavigationActivity.class);
                                startActivity(intent);

                            } else {

                                Toast.makeText(SplashActivity.this,"Token expired", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(i);
                            }
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            Toast.makeText(SplashActivity.this,"Internal server error.", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(i);
                        }
                    });
                } else {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                }


            }
        }, SPLASH_TIME_OUT);
    }




}
