package com.triangle.com.humano.Model;

import com.google.gson.annotations.SerializedName;
import com.triangle.com.humano.Scences.Attendance.ClockInActivity;

public class AttendanceModel {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lng")
    private String lng;

    @SerializedName("status")
    private String status;

    @SerializedName("clocktype")
    private String clocktype;



    public AttendanceModel(String lat, String lng, String status, String clocktype){

        this.lat = lat;
        this.lng = lng;
        this.status = status;
        this.clocktype = clocktype;




    }

    public String getLat() { return lat; }
    public void setLat(String lat) { this.lat = lat; }

    public String getLng() { return lng; }
    public void setLng(String lng) { this.lng = lng; }

    public String getStatus() { return status; }
    public void setStatus(String status) { this.lng = status; }

    public String getClocktype() { return clocktype; }
    public void setClocktype(String clocktype) { this.lng = clocktype; }


}
