package com.triangle.com.humano.Model;

import java.util.ArrayList;

public class SalaryModel {

    float score;
    String name;

    public SalaryModel(String name,float score) {
        this.score = score;
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<SalaryModel> getSampleStudentData(int size) {
        ArrayList<SalaryModel> student = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            student.add(new SalaryModel("Android v", (float) Math.random() * 100));
        }
        return student;
    }
}



