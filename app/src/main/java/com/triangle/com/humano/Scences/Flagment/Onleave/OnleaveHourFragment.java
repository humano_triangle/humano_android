package com.triangle.com.humano.Scences.Flagment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.Outside.OutSlideActivity;

import java.io.IOException;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnleaveHourFragment extends Fragment implements TimePickerDialog.OnTimeSetListener {

    Calendar calendar;
    DatePickerDialog datePickerDialog;

    RelativeLayout selectDate, selectReason, selectFile, selectTime;
    TextView selectDateResult,selectReasonResult, selectFileResult, selectTimeResult;
    ImageView imageView;

    public static final int PICK_IMAGE = 1;
    private static final int IMG_REQUEST = 777;
    private Bitmap bitmap;

    int day, month, year;


    public OnleaveHourFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_onleave_hour, container, false);

        selectDate = view.findViewById(R.id.onleave_relativelaout_hour_1);
        selectDateResult = view.findViewById(R.id.select_date_hour);

        selectReason = view.findViewById(R.id.onleave_relativelaout_hour_2);
        selectReasonResult = view.findViewById(R.id.select_reason_hour);

        selectFile = view.findViewById(R.id.onleave_relativelaout_hour_3);
        selectFileResult = view.findViewById(R.id.select_file_hour);

        selectTime = view.findViewById(R.id.onleave_relativelaout_hour_4);
        selectTimeResult = view.findViewById(R.id.select_time_hour);

        imageView = view.findViewById(R.id.imageView_hour);




        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDatePickerDialog();

            }
        });



        selectReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDialog();
            }
        });


        selectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        selectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTimePickerDialog();

            }
        });




        return view;
    }

    private void  setTimePickerDialog() {

        Calendar now = Calendar.getInstance();
        com.borax12.materialdaterangepicker.time.TimePickerDialog tpd = com.borax12.materialdaterangepicker.time.TimePickerDialog.newInstance(
                (com.borax12.materialdaterangepicker.time.TimePickerDialog.OnTimeSetListener) getActivity(),
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Log.d("TimePicker", "Dialog was cancelled");
            }
        });
        tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");






    }


    private void  setDatePickerDialog() {

        calendar = Calendar.getInstance();

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

                selectDateResult.setText(dayOfMonth + "/" +(month+1)+ "/" + year);

            }
        }, day, month, year);
        datePickerDialog.show();



    }


    private void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View subView = inflater.inflate(R.layout.dialog_onleave_reason, null);
        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Reason");
        builder.setMessage("Leave reason Message");
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectReasonResult.setText(subEditText.getText().toString());
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

    private void  selectImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, IMG_REQUEST);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_REQUEST && resultCode == Activity.RESULT_OK && data != null) {

            Uri path = data.getData();

            try {
                bitmap =  MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),path);
                imageView.setImageBitmap(bitmap);

                String resName = getResources().getResourceEntryName(R.id.imageView_hour);

                selectFileResult.setText(""+resName);



            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

    }
}
