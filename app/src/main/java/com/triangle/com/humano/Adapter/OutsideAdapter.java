package com.triangle.com.humano.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.triangle.com.humano.Model.OutsideModel;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.Outside.MainOutSideActivity;


import java.util.ArrayList;

public class OutsideAdapter extends RecyclerView.Adapter<OutsideAdapter.OutsideViewHolder>  {

    private ArrayList<OutsideModel> dataList;
    private Context context;
    private OnItemClickListener mListener;




    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public OutsideAdapter(Context context, ArrayList<OutsideModel> dataList) {

        this.context = context;
        this.dataList = dataList;
    }



    @Override
    public OutsideViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_outside, parent,false);

        return new OutsideViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OutsideViewHolder holder, int position) {

        OutsideModel model = dataList.get(position);
        String dateDate = model.getDate_Date();
        String dateTime = model.getDate_Time();
        String latitude = model.getLat();
        String longiTude = model.getLng();


        holder.txtDate.setText(dateDate);
        holder.txtTime.setText(dateTime);
        holder.txtLat.setText("Lat : " + latitude);
        holder.txtLng.setText("Lng: " + longiTude);


    }

    @Override
    public int getItemCount() {
        return  dataList.size() ;
    }



    public class OutsideViewHolder extends RecyclerView.ViewHolder  {

        public TextView txtDate, txtTime, txtLat, txtLng;
        public RelativeLayout relativeLayout;

       public OutsideViewHolder(final View itemView) {
            super(itemView);
            txtDate =  itemView.findViewById(R.id.txt_date);
            txtTime =  itemView.findViewById(R.id.txt_time);
            txtLat =  itemView.findViewById(R.id.txt_lat);
            txtLng = itemView.findViewById(R.id.txt_lng);
            relativeLayout = itemView.findViewById(R.id.relativeLayout_recyclerview);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);
                        }
                    }

                }
            });


//            relativeLayout.setOnClickListener(this);

        }

//        @Override
//        public void onClick(View v) {
//            int clickedPosition = getAdapterPosition();
//            Toast.makeText(context, dataList.get(clickedPosition).getDate_Date(), Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(context, CheckInActivity.class);
//            context.startActivity(intent);
//
//        }
    }

}
