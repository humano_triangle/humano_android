package com.triangle.com.humano.Scences.Onleave;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.triangle.com.humano.R;

public class MainOnleave2Activity extends AppCompatActivity {

    public static final String EXTRA_TYPE = "type";

    RelativeLayout onLeave, sickLeave, vacationLeave, otherLeave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_onleave2);


        onLeave = findViewById(R.id.relativeLayout_onleave);
        sickLeave = findViewById(R.id.relativeLayout_sickleave);
        vacationLeave = findViewById(R.id.relativeLayout_vacationleave);
        otherLeave = findViewById(R.id.relativeLayout_other);

        onLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), OnleaveActivity.class);
                intent.putExtra(EXTRA_TYPE, "Onleave");
                startActivity(intent);
            }
        });


        sickLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SickleaveActivity.class);
                intent.putExtra(EXTRA_TYPE, "Sickleave");
                startActivity(intent);
            }
        });

        vacationLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), VacationLeaveActivity.class);
                intent.putExtra(EXTRA_TYPE, "Vacationleave");
                startActivity(intent);
            }
        });

        otherLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), OtherleaveActivity.class);

                startActivity(intent);
            }
        });



    }
}
