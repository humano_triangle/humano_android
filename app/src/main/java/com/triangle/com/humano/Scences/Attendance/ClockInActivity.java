package com.triangle.com.humano.Scences.Attendance;

import android.Manifest;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.Interface.APIInterface;
import com.triangle.com.humano.Model.AttendanceModel;
import com.triangle.com.humano.Model.UserModel;
import com.triangle.com.humano.Network.RetrofitInstance;
import com.triangle.com.humano.R;



import retrofit2.Call;
import retrofit2.Callback;

public class ClockInActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 17f;

    private Boolean mLocationPermissionsGranted = false;
    private FusedLocationProviderClient mFusedLocationClient, client, fusedLocationProviderClient;
    private GoogleMap mMap;
    private BottomSheetBehavior mBottomSheetBehavior;
    private String locationCircle;
    private LatLng placeLatLng, getLatLng;
    private int statusLocation, clocktype ;

    LatLng destination ;
    RelativeLayout  borderMap ;
    ImageView imageView;
    ImageButton openCamera, goLocation, copy;
    TextView textViewlat, textViewlong, textViewlatlong, showLocation;
    Button comfirm;
    Helper helper;
    LocationManager locationManager;
    Context mContext;

    /*-------------EditRedius---------------*/

    int radiusGreen = 50, radiusYellow = 100;

    /*-------------------------------------*/





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_clock_in);

        getLocationPermission();

        showLocation = findViewById(R.id.show_location);
        goLocation = findViewById(R.id.btn_gps);
        comfirm = findViewById(R.id.btn_comfirm);
        borderMap = findViewById(R.id.border_map);


        helper = new Helper(getApplication());

        destination = new LatLng(13.769818, 100.611305);
//        destination = new LatLng(  13.6117233,100.7323332);


        System.out.println(" "+destination);

        //open Camera


        openCamera = findViewById(R.id.btn_open_camera);
        imageView =  findViewById(R.id.image_view);

        openCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,0);
            }
        });



        comfirm.setEnabled(false);


//        comfirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (placeLatLng != null){
//                    setLocationtoapi(placeLatLng, statusLocation);
//
//                }else {
//                    Toast.makeText(getApplicationContext(),"Internal server error", Toast.LENGTH_SHORT).show();
//
//                }
//
//
//            }
//        });




        mContext = this;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000,
                2, locationListenerGPS);




    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        getCurrentLocation();
        Circle();
        moveCamera(destination,DEFAULT_ZOOM);

        if (mLocationPermissionsGranted) {



            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }

        goLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
                getCurrentLocation();
            }
        });


    }


    public boolean checkInsetlocation(LatLng location, LatLng destination, int radius) {

        float[] distance = new float[2];
        Location.distanceBetween(location.latitude, location.longitude, destination.latitude, destination.longitude, distance);


        if (distance[0] > radius) {

            return false;

        } else {

            return true;
        }

    }

    LocationListener locationListenerGPS=new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {



            comfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (placeLatLng != null){

                        setLocationtoapi(placeLatLng, statusLocation);
//                        Toast.makeText(getApplicationContext(),placeLatLng+" " , Toast.LENGTH_SHORT).show();

//                        Toast.makeText(getApplicationContext(),placeLatLng+" : "+ statusLocation, Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(getApplicationContext(),"Require Data", Toast.LENGTH_SHORT).show();

                    }


                }
            });

            getLatLng = new LatLng(location.getLatitude(), location.getLongitude());


            Boolean isLocationInsetGreen = checkInsetlocation(destination, getLatLng, radiusGreen);
            Boolean isLocationInsetYellow = checkInsetlocation(destination, getLatLng, radiusYellow);

            if (isLocationInsetGreen) {

                statusLocation = 1 ;

                clocktype = 1;

                greenBlinkEffect();

                comfirm.setText("confirm");
                comfirm.setEnabled(true);


            } else if (isLocationInsetYellow ) {

                statusLocation = 2 ;
                clocktype = 1;

                yellowBlinkEffect();

                comfirm.setText("confirm");
                comfirm.setEnabled(true);


            }else {

                statusLocation = 3;
                clocktype = 1;

                Toast.makeText(getApplicationContext(), "Out!!!", Toast.LENGTH_SHORT).show();
                redBlinkEffect();

                comfirm.setText("confirm");
                comfirm.setEnabled(true);


            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = (Bitmap)data.getExtras().get("data");
        imageView.setImageBitmap(bitmap);
    }



    private void greenBlinkEffect() {
        ObjectAnimator animatorGreen = ObjectAnimator.ofInt(borderMap, "backgroundColor", Color.WHITE, Color.GREEN, Color.WHITE);
        animatorGreen.setDuration(600);
        animatorGreen.setEvaluator(new ArgbEvaluator());
        animatorGreen.setRepeatMode(Animation.REVERSE);
        animatorGreen.setRepeatCount(Animation.INFINITE);
        animatorGreen.start();
    }
    private void yellowBlinkEffect() {
        ObjectAnimator animatorGreen = ObjectAnimator.ofInt(borderMap, "backgroundColor", Color.WHITE, Color.YELLOW, Color.WHITE);
        animatorGreen.setDuration(600);
        animatorGreen.setEvaluator(new ArgbEvaluator());
        animatorGreen.setRepeatMode(Animation.REVERSE);
        animatorGreen.setRepeatCount(Animation.INFINITE);
        animatorGreen.start();
    }
    private void redBlinkEffect() {
        ObjectAnimator animatorGreen = ObjectAnimator.ofInt(borderMap, "backgroundColor", Color.WHITE, Color.RED, Color.WHITE);
        animatorGreen.setDuration(600);
        animatorGreen.setEvaluator(new ArgbEvaluator());
        animatorGreen.setRepeatMode(Animation.REVERSE);
        animatorGreen.setRepeatCount(Animation.INFINITE);
        animatorGreen.start();
    }

    private void getCurrentLocation() {

        client = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            return;
        }

        client.getLastLocation().addOnSuccessListener(ClockInActivity.this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {

                    placeLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                }
            }
        });


    }
    private void getDeviceLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (mLocationPermissionsGranted) {
                Task location = mFusedLocationClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = (Location) task.getResult();

                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);
                        } else {
                            Toast.makeText(ClockInActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        } catch (SecurityException e) {

        }
    }
    private void Circle() {

        mMap.addCircle(new CircleOptions().center(destination).radius(radiusGreen).strokeColor(Color.GREEN));
        mMap.addCircle(new CircleOptions().center(destination).radius(radiusYellow).strokeColor(Color.YELLOW));
        System.out.println("Circlr: "+destination);

    }
    private void moveCamera(LatLng destination, float defaultZoom) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination, defaultZoom));


    }
    private void initMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map3);

        mapFragment.getMapAsync(ClockInActivity.this);
    }
    private void getLocationPermission(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(),FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(),COURSE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                mLocationPermissionsGranted = true;
                initMap();

            }else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if (grantResults.length >0 ){
                    for (int i = 0;i < grantResults.length; i++){
                        if ( grantResults[0] == PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionsGranted = false;
                            return;
                        }

                    }
                    mLocationPermissionsGranted = true;
                    initMap();

                }

            }
        }
    }





    private void setLocationtoapi(LatLng latLng, int statusLocation ) {

        final String lat = ""+latLng.latitude;
        final String lng = ""+latLng.longitude;
        final String status = ""+statusLocation;
        final String clocktype = "Clock-in";

        APIInterface apiInterface = RetrofitInstance
                .getRetrofitInstance(getApplicationContext())
                .create(APIInterface.class);
        Call<AttendanceModel> call = apiInterface.setClockin(lat, lng, status, clocktype, helper.getUsername(), helper.getUserData().getToken() );
        call.enqueue(new Callback<AttendanceModel>() {
            @Override
            public void onResponse(Call<AttendanceModel> call, retrofit2.Response<AttendanceModel> response) {

//                System.out.println(" "+response);
//                System.out.println(" "+call);

                if (response.isSuccessful()){
                    ClockInActivity.super.onBackPressed();

                    UserModel model = helper.getUserData();

                    Toast.makeText(getApplicationContext(), "Sucess", Toast.LENGTH_SHORT).show();



                    System.out.println(" "+helper.getUserData().getToken());
                    System.out.println(" "+helper.getUsername());
                    System.out.println(" "+lat);
                    System.out.println(" "+lng);
                    System.out.println("status: "+status);
                    System.out.println(" "+clocktype);

//                    Toast.makeText(getApplicationContext(), ""+model.getToken(),Toast.LENGTH_SHORT).show();
//
//                    Toast.makeText(getApplicationContext(), ""+helper.getUsername() , Toast.LENGTH_SHORT).show();


                }else {
                    Toast.makeText(getApplicationContext(), "Token require", Toast.LENGTH_SHORT).show();
                    ClockInActivity.super.onBackPressed();


                }

            }

        @Override
        public void onFailure(Call<AttendanceModel> call, Throwable t) {

            System.out.println(" "+helper.getUserData().getToken());
            System.out.println(" "+helper.getUsername());
            System.out.println(" "+lat);
            System.out.println(" "+lng);

            System.out.println(" "+call);
            System.out.println(" "+t);

            Toast.makeText(getApplicationContext(),"Internal server error", Toast.LENGTH_SHORT).show();
            ClockInActivity.super.onBackPressed();

        }
    });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationManager != null){
            //noinspection MissingPermission
            locationManager.removeUpdates(locationListenerGPS);
        }
    }


}
