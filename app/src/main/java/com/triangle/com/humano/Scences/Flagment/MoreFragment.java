package com.triangle.com.humano.Scences.Flagment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.triangle.com.humano.Helper.Helper;
import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.MainActivity;
import com.triangle.com.humano.Scences.SecondActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {

    Helper helper;
    TextView empNameTextView, empIdTextView, empPhoneTextView, empSalaryView, logOut, changLanguage;
    CircleImageView empimageView;


    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_more, container, false);


        empimageView = rootView.findViewById(R.id.emp_imageView);
        empIdTextView = rootView.findViewById(R.id.emp_id_text);
        empNameTextView = rootView.findViewById(R.id.emp_name_textView);
        logOut = rootView.findViewById(R.id.log_out);
        changLanguage = rootView.findViewById(R.id.setting);

        changLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SecondActivity.class);
                startActivity(intent);
            }
        });

        helper = new Helper(getActivity());
        setupComponent();



        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });



        return rootView;
    }






    private void setupComponent() {


        String prefix = "http://"+helper.getHostData();
        String empName = helper.getUserData().getName();
        String empSName = helper.getUserData().getSname();
        String empId = helper.getUserData().getEmcode();
//        String empPhone = helper.getUserData().getPhoneNumber();
        String empImageURL = prefix+helper.getUserData().getImage();
//        Double empSalary = helper.getUserData().getSalary();

        System.out.println("imagePath: "+empImageURL);

        Picasso.get().load(empImageURL).into(empimageView);
//        empPhoneTextView.setText("Phone: "+empPhone);
        empNameTextView.setText(empName + " " + empSName);
        empIdTextView.setText(empId);
//        empSalaryView.setText(""+empSalary);




    }




}
