package com.triangle.com.humano.Scences.Flagment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.triangle.com.humano.R;
import com.triangle.com.humano.Scences.CalendaActivity;
import com.triangle.com.humano.Scences.OnleaveActivity;
import com.triangle.com.humano.Scences.Outside.MainOutSideActivity;
import com.triangle.com.humano.Scences.Attendance.AttendanceActivity;
import com.triangle.com.humano.Scences.SecondActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class EsmFragment extends Fragment {


    public EsmFragment() {
        // Required empty public constructor

    }


    RelativeLayout btnCalenda, btnOutside, btnAttendance, btnOnleave, btnTraning, btnBanefit, btnSalary, btnAppraisal, btnOvertime;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_esm, container, false);
        btnCalenda = view.findViewById(R.id.calendar);
        btnOutside = view.findViewById(R.id.outside);
        btnAttendance = view.findViewById(R.id.attendance);
        btnOnleave = view.findViewById(R.id.onleave);
//        btnTraning = view.findViewById(R.id.traning);
//        btnSalary = view.findViewById(R.id.salary);
        btnAppraisal = view.findViewById(R.id.approved);
        btnOvertime = view.findViewById(R.id.overtime);








        btnCalenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getActivity(),CalendaActivity.class);
                startActivity(in);
            }
        });

        btnOutside.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainOutSideActivity.class);
                startActivity(intent);


            }
        });

        btnAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AttendanceActivity.class);
                startActivity(intent);
            }
        });

        btnOnleave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), OnleaveActivity.class);
                startActivity(intent);

            }
        });
//
//        btnTraning.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent = new Intent(getActivity(), SecondActivity.class);
////                startActivity(intent);
//                Toast.makeText(getActivity(), "Coming Soon ...", Toast.LENGTH_SHORT).show();
//            }
//        });



//        btnSalary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Coming Soon ...", Toast.LENGTH_SHORT).show();
//            }
//        });

        btnAppraisal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon ...", Toast.LENGTH_SHORT).show();
            }
        });

        btnOvertime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Coming Soon ...", Toast.LENGTH_SHORT).show();
            }
        });


     return view;
    }





}
