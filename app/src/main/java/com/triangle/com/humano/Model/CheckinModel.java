package com.triangle.com.humano.Model;

import com.google.gson.annotations.SerializedName;

public class CheckinModel {

    @SerializedName("lat")
    private String lat;

    @SerializedName("lng")
    private String lng;


    @SerializedName("date_start")
    private String date_start;

    @SerializedName("date_end")
    private String date_end;

    @SerializedName("time_start")
    private String time_start;

    @SerializedName("time_end")
    private String time_end;


    @SerializedName("lang")
    private String lang;

    public CheckinModel(String lat, String lng, String date_start, String date_end, String time_start, String time_end , String lang){

        this.lat = lat;
        this.lng = lng;

        this.date_start = date_start;
        this.date_end = date_end;
        this.time_start = time_start;
        this.time_end = time_end;
        this.lang = lang;





    }

    public String getLat() { return lat; }
    public void setLat(String lat) { this.lat = lat; }

    public String getLng() { return lng; }
    public void setLng(String lng) { this.lng = lng; }


    public String getDate_start() { return date_start; }
    public void setDate_start(String date_start) { this.date_start = date_start; }

    public String getDate_end() { return date_end; }
    public void setDate_end(String date_end) { this.date_end = date_end; }


    public String getTime_start() { return time_start; }
    public void setTime_start(String time_start) { this.time_start = time_start; }

    public String getTime_end() { return time_end; }
    public void setTime_end(String time_end) { this.time_end = time_end; }

    public String getLang() { return lang; }
    public void setLang(String lang) { this.lang = lang; }



}
